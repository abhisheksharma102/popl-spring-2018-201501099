#lang racket

(require eopl)

(define-datatype bst bst?
  [null-node]
  [node (val number?)
        (left bst?)
        (right bst?)]
)


(define build-tree
    (lambda (args)
        (letrec 
            (
                [add-node
                    (lambda (BST value)
                        (cases bst BST
                            (null-node ()
                                (node value (null-node) (null-node))
                            )
                            (node (num left right)
                                (
                                    if (> num value)
                                        (node num (add-node left value) right)
                                        (node num left (add-node right value))
                                )
                            )
                        )
                    )
                ]
                [iterate-list
                    (lambda (el-list tree-till-now)
                        (if [> (length el-list) 0]
                            (begin
                                (set! tree-till-now (add-node tree-till-now (first el-list)))
                                (iterate-list (rest el-list) tree-till-now)
                            )
                            tree-till-now
                        )
                    )
                ]
            )
            (iterate-list args (null-node))
        )
    )
)

(provide build-tree)
;(build-tree '(3 1 4 2))